const html = arg => arg.join('');

let dragging = { value: null };
export default {
  template: html`
    <div class="batch-preview">
      <div class="batch-header">
        {{batch.length}} files
        <button @click="sort('alphabetical')">Sort (A-Z)</button>
        <button @click="sort('number')">Sort (number)</button>
      </div>
      <div class="batch-item" v-for="(item, i) in batch" >
        {{ item.filename }}
        <button
          :disabled="Math.floor(i / batchStep) == batchPosition"
          @click="$emit('select', Math.floor(i / batchStep))"
        >view</button>
        <button
          :disabled="i >= batch.length"
          @click="move(item, 1)"
        >down</button>
        <button
          :disabled="i == 0"
          @click="move(item, -1)"
        >up</button>
      </div>
    </div>
  `,
  data: () => ({ dragging }),
  props: ['batch', 'batchPosition', 'batchStep'],
  methods: {
    sort(algo) {
      let sorted = this.batch.sort(({ filename: a }, { filename: b }) => {
        switch (algo) {
          case 'alphabetical': return a == b ? 0 : (a > b ? 1 : -1);
          case 'number':
            a = parseInt(a.replace(/[^\d]/g, ''));
            b = parseInt(b.replace(/[^\d]/g, ''));
            return a == b ? 0 : (a > b ? 1 : -1);
        }
      });
      this.batch.splice(0, this.batch.length, ...sorted);
    },
    move(item, delta) {
      let index = this.batch.indexOf(item);
      this.batch.splice(index, 1);
      let target = index + delta;
      if (target < 0) target = 0;
      if (target >= this.batch.length) target = this.batch.length-1;
      this.batch.splice(target, 0, item);
    }
  }
}

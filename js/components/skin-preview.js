import SkinSplicer from '../splicers/skin-splicer.js'

const g = 'ghost';
const gb = 'gb';
const PCO_MAP = [
  [['z', 0b00100], ['z', 0b10011], ['_', 0b00000], ['_', 0b00000], ['_', 0b00000], ['_', 0b00000], ['i', 0b00100], ['i', 0b00101], ['i', 0b00101], ['i', 0b00001]],
  [['t', 0b00010], ['z', 0b11100], ['z', 0b00001], ['_', 0b00000], ['_', 0b00000], [ g , 0b00010], ['l', 0b00010], ['o', 0b00110], ['o', 0b00011], ['j', 0b00010]],
  [['t', 0b11110], ['t', 0b00001], ['s', 0b10110], ['s', 0b00001], [ g , 0b00100], [ g , 0b11011], ['l', 0b01010], ['o', 0b01100], ['o', 0b01001], ['j', 0b01010]],
  [['t', 0b01000], ['s', 0b00100], ['s', 0b11001], ['_', 0b00000], ['_', 0b00000], [ g , 0b01000], ['l', 0b11100], ['l', 0b00001], ['j', 0b00100], ['j', 0b11001]],
  [[ gb, 0b00100], [ gb, 0b00101], [ gb, 0b00101], [ gb, 0b00001], ['_', 0b00000], [ gb, 0b00100], [ gb, 0b00101], [ gb, 0b00101], [ gb, 0b00101], [ gb, 0b00001]],
];

const html = arg => arg.join('');
export default {
  template: html`
    <div class="cmp-skin-preview">
      <canvas ref="rendered" :style="style"></canvas>
      <canvas ref="texture" :style="style"></canvas>
    </div>
  `,
  props: ['splicer', 'rendering', 'rounding'],
  mounted() {
    this.redraw();
  },
  watch: {
    splicer() { this.redraw() }
  },
  computed: {
    style() {
      return {
        filter: `brightness(${this.rendering.brightness*100}%)`
      }
    }
  },
  methods: {
    redraw() {
      let canvas = this.$refs.rendered;
      if (!canvas) return;

      let ctx = canvas.getContext('2d');
      // ctx.imageSmoothingEnabled = false;
      if (!this.splicer) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        return;
      }

      const map = PCO_MAP;
      this.hasMinos = !!this.splicer.get('z');
      this.hasGhost = !!this.splicer.get('ghost');

      let roundingOp = this.rounding ? Math.floor : x => x;

      let { width, height } = canvas.getBoundingClientRect();
      height = roundingOp(width * map.length/map[0].length);
      width = roundingOp(width);
      canvas.width = width;
      canvas.height = height;

      for (let y = 0; y < map.length; y++) {
        for (let x = 0; x < map[y].length; x++) {
          let [piece, conn] = map[y][x];
          if (piece == '_') continue;

          let dest = [
            x * width/map[y].length,
            y * height/map.length,
            width / map[y].length+1,
            height / map.length+1
          ].map(coord => roundingOp(coord));
          ctx.clearRect(...dest);

          for (let tex of (this.splicer.get(piece, conn) || [])) {
            ctx.globalAlpha = this.selected && piece == 'ghost' ? 0.25 : 1;

            let padding = this.splicer.format.startsWith('tetrio61')
              ? this.splicer.orderedImages[0].width >= 2048 ? 2 : 1
              : 0;
            tex[1] = roundingOp(tex[1] + padding);
            tex[2] = roundingOp(tex[2] + padding);
            tex[3] = roundingOp(tex[3] - padding*2);
            tex[4] = roundingOp(tex[4] - padding*2);
            ctx.drawImage(...tex, ...dest);
          }
        }
      }

      let texCanvas = this.$refs.texture;
      let srcImg = this.splicer.images[0];
      texCanvas.width = srcImg.width;
      texCanvas.height = srcImg.height;
      let texCtx = texCanvas.getContext('2d');
      texCtx.drawImage(srcImg, 0, 0);
    }
  }
}

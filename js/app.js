/* Author: UniQMG */

import SkinPreview from './components/skin-preview.js';
import BatchEditor from './components/batch-editor.js';

import TwelveBlocksSplicer from './splicers/12-blocks-splicer.js';
import BackgroundAndBorderSplicer from './splicers/bg-border-splicer.js';
import SkinSplicer, * as maps from './splicers/skin-splicer.js'
import TwentySplicer from './splicers/twenty-splicer.js';
import TetrioGapFixerSplicer from './splicers/tetrio-gap-fixer-splicer.js';

const TARGET_SIZES = { // in multiples of block size
  tetrioraster:           { w: 12.4, h:  1.0 },
  jstrisraster:           { w:  9.0, h:  1.0 },
  tetrio61:               { w:  256/48, h:  256/48 },
  tetrio61ghost:          { w:  128/48, h:  128/48 },
  tetrio61connected:      { w: 1024/48, h: 1024/48 },
  tetrio61connectedghost: { w:  512/48, h:  512/48 }
};
const PIECE_COLORS = {
  z: 0xFF0000,
  l: 0xFF8000,
  o: 0xFFFF00,
  s: 0x2BFF00,
  i: 0x00FFEA,
  j: 0x0000FF,
  t: 0xFF00FF,
  hold: 0x444444,
  gb: 0x555555,
  dgb: 0x333333,
  ghost: 0xFFFFFF,
  topout: 0xFF0000
};
const FORMATS = {
  source: [
    '12-blocks',
    '20-corners',
    'background-and-border',
    // 'tetriosvg',
    'tetrioraster',
    // 'tetrioanim',
    'jstrisraster',
    // 'jstrisanim',
    'jstrisconnected',
    'tetrio61',
    'tetrio61ghost',
    // 'tetrio61multi',
    'tetrio61connected',
    // 'tetrio61connectedanimated',
    'tetrio61connectedghost',
    // 'tetrio61connectedghostanimated',
    // 'tetrio61connectedmulti'
  ],
  target: Object.keys(TARGET_SIZES)
};

const ALL_PIECES = [
  ...Object.keys(maps.TETRIO_61_CONN_MAP),
  ...Object.keys(maps.TETRIO_61_CONN_GHOST_MAP)
];
const ALL_CONNS = Object.keys(maps.TETRIO_CONNECTIONS_SUBMAP);

const html = arg => arg.join('');
const app = new Vue({
  template: html`
    <div class="cmp-skin-tool">
      <div class="before controls">
        <input type="file" multiple accept="image/*" @change="uploaded" />
        <div>
          <label for="input-format">Format</label>
          <select name="input-format" v-model="format.source">
            <option v-for="format in formats.source" :value="format">
              {{ format }}
            </option>
          </select>
        </div>
        <button @click="showBatch = true" v-if="!showBatch">Show batch</button>
        <button @click="showBatch = false" v-else>Preview</button>
      </div>
      <div class="after controls">
        <div>
          <label for="resolution">Block resolution</label>
          <input name="resolution" type="number" min="1" max="256" v-model.number="resolution" />
          <button @click="autoResolution" :disabled="!splicer">Auto</button>
        </div>
        <div>
          <label for="output-format">Format</label>
          <select name="output-format" v-model="format.target">
            <option v-for="format in formats.target" :value="format">
              {{ format }}
            </option>
          </select>
        </div>
        <button @click="download" :disabled="!targetSplicer || status">
          Download
        </button>
      </div>
      <batch-editor
        class="before"
        v-if="showBatch"
        :batch="batch"
        :batchPosition="batchPosition"
        :batchStep="imagesPerOutput"
        @select="batchPosition = $event"
      />
      <skin-preview class="before" v-else v-bind="{
        splicer: splicer,
        rendering: rendering,
        rounding: output.rounding
      }"/>
      <div class="gap"/>
      <skin-preview class="after" v-bind="{
        splicer: targetSplicer,
        rendering: rendering,
        rounding: output.rounding
      }"/>
      <div class="after controls bottom">
        <strong>Additional output options</strong>
        <span v-if="status" style="float: right">({{status}})</span>
        <div class="output-controls">
          <div class="control-group">
            <label for="padding">Add padding</label>
            <span>
              <input
                name="padding"
                v-model="output.padding"
                min="0"
                type="number"
                style="max-width: 120px"
              />px
            </span>
          </div>
          <div class="control-group">
            <label for="paddingover">Draw unpadded</label>
            <input name="paddingover" type="checkbox" v-model="output.paddingover" />
          </div>
          <div class="control-group">
            <label for="grayscale">Grayscale</label>
            <input name="grayscale" type="checkbox" v-model="output.grayscale" />
          </div>
          <div class="control-group">
            <label for="colorize">Colorize</label>
            <select name="colorize" v-model="output.colorize">
              <option value="off">OFF</option>
              <option value="multiply">Multiply</option>
              <option value="overlay">Overlay</option>
              <option value="darken">Darken</option>
              <option value="lighten">Lighten</option>
              <option value="color-burn">color-burn</option>
              <option value="hue">hue</option>
            </select>
          </div>
          <div class="control-group">
            <label for="interpolate">Smoothing</label>
            <input name="interpolate" type="checkbox" v-model="output.interpolate" />
          </div>
        </div>
        <div class="output-controls single" v-if="format.source == 'tetrioraster'">
          <div class="control-group">
            <label for="gapCorrectionEnabled">Gap correction</label>
            <div>
              <input
                name="gapCorrectionEnabled"
                type="checkbox"
                v-model="output.gapCorrectionEnabled"
              />
              <input
                name="gapCorrectionEnabled"
                :disabled="!output.gapCorrectionEnabled"
                type="number"
                style="max-width: 120px"
                v-model.number="output.gapCorrection"
              />px
            </div>
          </div>
        </div>
      </div>
      <div class="bottom controls">
        <label for="brightness">Brightness (display only, for eyestrain)</label>
        <input name="brightness" v-model="rendering.brightness" type="range" min="0" max="1" step="0.01" />
      </div>
    </div>
  `,
  components: { SkinPreview, BatchEditor },
  data: {
    formats: FORMATS,
    format: {
      source: 'tetrio61connected',
      target: 'tetrio61connected'
    },

    batch: [],
    batchPosition: 0,
    showBatch: false,

    resolution: 48, // pixels per block
    rendering: { brightness: 1 },
    output: {
      gapCorrectionEnabled: false,
      gapCorrection: 0,
      padding: 0,
      paddingover: false,
      grayscale: false,
      colorize: 'off',
      rounding: false,
      interpolate: false
    },

    targetSplicer: null,
    targetSplicerPromise: null,
    recreateTargetSplicerWhenDone: false,

    targetSplicerRenderProgress: null,
    batchRenderProgress: null
  },
  computed: {
    splicer() {
      let skins = this.partitionedBatches[this.batchPosition];
      console.log('recompute splicer', this.batchPosition, this.partitionedBatches, skins);
      if (!skins || skins.length == 0) return;
      let skin = skins[0];
      let opts = {
        rounding: this.output.rounding,
        interpolate: this.output.interpolate
      };
      switch (this.format.source) {
        case '12-blocks':
          return new TwelveBlocksSplicer(skins, opts);
        case '20-corners':
          return new TwentySplicer(skin, opts);
        case 'background-and-border':
          return new BackgroundAndBorderSplicer(skin, opts);
        case 'tetrioraster':
          if (this.output.gapCorrectionEnabled)
            return new TetrioGapFixerSplicer(skin, this.output.gapCorrection);
          return new SkinSplicer(this.format.source, [skin], opts);
        default:
          return new SkinSplicer(this.format.source, [skin], opts);
      }
    },
    status() {
      if (this.batchRenderProgress) {
        return typeof this.batchRenderProgress == 'number'
          ? `Rendering ${this.batchRenderProgress} / ${this.batch.length}`
          : `Rendering: ${this.batchRenderProgress}`;
      }
      if (this.targetSplicerRenderProgress)
        return `Rendering block #${this.targetSplicerRenderProgress}`;
      return null;
    },
    imagesPerOutput() {
      switch (this.format.source) {
        case '12-blocks': return 12;
        default: return 1;
      }
    },
    partitionedBatches() {
      let batches = [];
      for (let i = 0; i < Math.ceil(this.batch.length / this.imagesPerOutput); i++) {
        batches.push(this.batch.slice(i*this.imagesPerOutput, (i+1)*this.imagesPerOutput));
      }
      return batches;
    }
  },
  watch: {
    splicer() { this.resetTargetSplicer() },
    resolution() { this.resetTargetSplicer() },
    rendering: { deep: true, handler() { this.resetTargetSplicer() } },
    output: { deep: true, handler() { this.resetTargetSplicer() } },
    ['format.target']: function() { this.resetTargetSplicer() }
  },
  methods: {
    autoResolution() {
      this.resolution = this.splicer.get('z')[0][3];
    },
    async resetTargetSplicer() {
      if (!this.splicer) {
        console.warn("resetTargetSplicer: No splicer");
        return;
      }
      if (this.targetSplicerRenderProgress) {
        this.recreateTargetSplicerWhenDone = true;
        return;
      }

      let resolve = null;
      this.targetSplicerPromise = new Promise(res => resolve = res);

      this.targetSplicerRenderProgress = 1;
      this.targetSplicer = await this.createTargetSplicer(frame => {
        this.targetSplicerRenderProgress++;
      });
      resolve(this.targetSplicer);

      this.targetSplicerRenderProgress = null;
      if (this.recreateTargetSplicerWhenDone) {
        this.recreateTargetSplicerWhenDone = false;
        await this.resetTargetSplicer();
      }
    },
    async createTargetSplicer(onFrame) {
      if (!this.splicer) return;

      let lastBreak = Date.now();

      let canvas = document.createElement('canvas');
      let { w, h } = TARGET_SIZES[this.format.target];
      canvas.width = w * this.resolution;
      canvas.height = h * this.resolution;
      const target = new SkinSplicer(this.format.target, [canvas], {
        rounding: this.output.rounding,
        interpolate: this.output.interpolate
      });
      const source = this.splicer;

      for (let piece of ALL_PIECES) {
        for (let conns of ALL_CONNS) {
          let drawCalls = source.get(piece, conns);
          if (!drawCalls || !drawCalls.length) continue;
          let targetRes = (target.get(piece.conns) || drawCalls[0]).slice(3, 5);

          const temp = document.createElement('canvas');
          let ctx = temp.getContext('2d');
          temp.width = targetRes[0];
          temp.height = targetRes[1];
          ctx.clearRect(0, 0, w, h);

          if (this.output.paddingover && this.output.padding)
            for (let [tex, x, y, w, h] of drawCalls)
              ctx.drawImage(tex, x, y, w, h, 0, 0, w, h);

          for (let [tex, x, y, w, h] of drawCalls) {
            let p = this.output.padding;
            ctx.drawImage(tex, x, y, w, h, p, p, w-p*2, h-p*2);
          }


          if (this.output.grayscale) {
            let image = ctx.getImageData(0, 0, temp.width, temp.height);
            for (let i = 0; i < image.data.length; i += 4) {
              let [r, g, b, a] = image.data.slice(i, i+4);
              let grey = (0.2126*r/255 + 0.7152*g/255 + 0.0722*b/255) * 255;
              image.data[i+0] = grey;
              image.data[i+1] = grey;
              image.data[i+2] = grey;
            }
            ctx.putImageData(image, 0, 0);
          }

          if (this.output.colorize != 'off') {
            let uncolorized = document.createElement('canvas');
            uncolorized.width = temp.width;
            uncolorized.height = temp.height;
            let unCtx = uncolorized.getContext('2d');
            unCtx.clearRect(0, 0, uncolorized.width, uncolorized.height);
            unCtx.drawImage(temp, 0, 0);

            let tr = (PIECE_COLORS[piece] >> 16 & 0xFF);
            let tg = (PIECE_COLORS[piece] >> 8 & 0xFF);
            let tb = (PIECE_COLORS[piece] & 0xFF);
            ctx.fillStyle = `rgb(${tr}, ${tg}, ${tb})`;
            ctx.globalCompositeOperation = this.output.colorize;
            ctx.fillRect(0, 0, temp.width, temp.height);

            ctx.globalCompositeOperation = 'destination-in';
            ctx.drawImage(uncolorized, 0, 0);
          }

          let res = target.set(piece, conns, temp);
          if (onFrame) onFrame();

          if (Date.now() > lastBreak + 100) {
            await new Promise(res => setTimeout(res));
            lastBreak = Date.now();
            console.log("Yielding at", Date.now());
          }
        }
      }

      return target;
    },
    downloadUrl(filename, url) {
      let blob = url instanceof Blob;

      let a = document.createElement('a');
      a.style.display = "none";
      a.download = filename;
      a.href = blob ? URL.createObjectURL(url) : url;

      document.body.appendChild(a);
      a.click();

      setTimeout(() => {
        a.remove();
        if (blob) URL.revokeObjectURL(a.href);
      }, 100);
    },
    async download() {
      if (this.partitionedBatches.length == 1) {
        let [_, filename, ext] = /(.+?)(\.(.+)?|$)/.exec(this.partitionedBatches[0][0].filename);

        this.targetSplicerPromise = null;
        this.resetTargetSplicer();
        while (!this.targetSplicerPromise) {
          await new Promise(res => setTimeout(res, 50));
        }
        
        let splicer = await this.targetSplicerPromise;
        this.downloadUrl(
          `${filename}_${this.format.target}${ext}`,
          splicer.images[0].toDataURL('image/png')
        );
      } else {
        let zip = new JSZip();
        for (let i = 0; i < this.partitionedBatches.length; i++) {
          console.log(`Rendering ${i}`)
          await new Promise(res => setTimeout(res, 50));

          this.batchPosition = i;
          this.batchRenderProgress = i * this.imagesPerOutput;

          this.targetSplicerPromise = null;
          this.resetTargetSplicer();
          while (!this.targetSplicerPromise) {
            await new Promise(res => setTimeout(res, 50));
          }

          let splicer = await this.targetSplicerPromise;
          let dataUrl = splicer.images[0].toDataURL('image/png');
          let b64Index = dataUrl.indexOf('base64,') + 'base64,'.length;
          zip.file(
            this.partitionedBatches[i][0].filename,
            dataUrl.slice(b64Index),
            { base64: true }
          );
        }
        this.batchRenderProgress = 'generating zip';
        this.downloadUrl(
          `batch_${this.partitionedBatches[0][0].filename}_${this.format.target}.zip`,
          await zip.generateAsync({ type: 'blob' })
        );
      }

      this.batchPosition = 0;
      this.batchRenderProgress = null;
    },
    async uploaded(evt) {
      for (let img in this.batch.splice(0))
        URL.revokeObjectURL(img.src);

      this.batch.push(...await Promise.all([...evt.target.files].map(file => {
        let img = new Image();

        let filekeys = {
          _connected_minos: "tetrio61connected",
          _unconnected_minos: "tetrio61",
          _connected_ghost: "tetrio61connectedghost",
          _unconnected_ghost: "tetrio61ghost"
        };

        for (let [key, format] of Object.values(filekeys))
          if (file.name.indexOf(key) != -1)
            this.format.input = format;

        for (let format of this.formats.source)
          if (file.name.indexOf(format) != -1)
            this.format.input = format;

        return new Promise(res => {
          img.filename = file.name;
          img.onload = () => res(img);
          img.src = URL.createObjectURL(file);
        });
      })));
      console.log("Created batch", this.batch);
      this.batchPosition = 0;
    }
  }
});
app.$mount('#app');
window.app = app;

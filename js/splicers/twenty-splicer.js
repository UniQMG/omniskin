const MAP_W = 10;
const MAP_H = 2;
export const CONN_SUBMAP = {
  // key = corner (T, L, J, S, Z), top, right, bottom, left (1=open,0=closed)
  // value = 2x2 border set to pull from (0-4), starting at top left going clockwise
  0b00010: [0, 0, 2, 2],
  0b00110: [0, 1, 4, 2],
  0b00111: [1, 1, 4, 4],
  0b00011: [1, 0, 2, 4],
  0b01010: [2, 2, 2, 2],
  0b01110: [2, 4, 4, 2],
  0b01111: [4, 4, 4, 4],
  0b01011: [4, 2, 2, 4],
  0b01000: [2, 2, 0, 0],
  0b01100: [2, 4, 1, 0],
  0b01101: [4, 4, 1, 1],
  0b01001: [4, 2, 0, 1],
  0b00000: [0, 0, 0, 0],
  0b00100: [0, 1, 1, 0],
  0b00101: [1, 1, 1, 1],
  0b00001: [1, 0, 0, 1],
  0b10110: [0, 1, 3, 2],
  0b10011: [1, 0, 2, 3],
  0b11101: [3, 3, 1, 1],
  0b11110: [2, 3, 3, 2],
  0b11100: [2, 3, 1, 0],
  0b11001: [3, 2, 0, 1],
  0b10111: [1, 1, 3, 3],
  0b11011: [3, 2, 2, 3]
};
export default class TwentySplicer {
  constructor(image, options={}) {
    this.format = '20-corners';
    this.image = image;
    this.options = Object.assign({ rounding: false }, options);
  }

  get images() { return [this.image]; }
  stats() { throw new Error("Not implemented"); }
  set() { throw new Error("Not implemented"); }

  get(piece, connection) {
    const ORDINAL = [[0, 0], [1, 0], [1, 1], [0, 1]];
    let conn = CONN_SUBMAP[connection] || CONN_SUBMAP[0];

    return conn.map((set, i) => {
      let x = set * 2 + ORDINAL[i][0];
      let y = ORDINAL[i][1];
      let w = this.image.width / MAP_W;
      let h = this.image.height / MAP_H;
      let roundingOp = this.options.rounding ? Math.floor : x => x;

      let canvas = document.createElement('canvas');
      canvas.width = Math.floor(w*2);
      canvas.height = Math.floor(h*2);
      canvas.getContext('2d').drawImage(
        this.image, ...[
          x * w, y * h, w, h,
          ORDINAL[i][0] * w, ORDINAL[i][1] * h, w, h
        ].map(e => roundingOp(e))
      );

      return [canvas, 0, 0, canvas.width, canvas.height];
    })
  }
}

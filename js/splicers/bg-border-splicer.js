import { TETRIO_CONNECTIONS_SUBMAP } from './skin-splicer.js'

export default class TwentySplicer {
  constructor(image) {
    this.format = 'background-and-border';
    this.image = image;
  }

  get images() { return [this.image]; }
  stats() { throw new Error("Not implemented"); }
  set() { throw new Error("Not implemented"); }

  clipLayers(image, xOffset, clipTest, clipCallback) {
    let canvas = document.createElement('canvas');
    canvas.width = image.width/3;
    canvas.height = image.height;
    let ctx = canvas.getContext('2d');

    ctx.fillStyle = 'white';
    for (let i = 0; i < 4; i++) {
      if (!clipTest(i)) continue;
      ctx.save();
      ctx.translate(canvas.width/2, canvas.height/2);
      ctx.rotate(i * Math.PI/2);
      ctx.translate(-canvas.width/2, -canvas.height/2);
      clipCallback(ctx, canvas.width, canvas.height);
      ctx.restore();
    }

    ctx.globalCompositeOperation = 'source-in';
    ctx.drawImage(
      this.image,
      xOffset*image.width/3, 0, image.width/3, image.height,
      0, 0, canvas.width, canvas.height
    );

    return canvas;
  }

  get(piece, connection=0) {
    let [[x, y]] = TETRIO_CONNECTIONS_SUBMAP[connection] || TETRIO_CONNECTIONS_SUBMAP[0];
    let { width: w, height: h } = this.image;

    let borders = this.clipLayers(
      this.image, 1,
      i => (connection & (0b1000 >> i)) == 0,
      (ctx, w, h) => {
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(w, 0);
        ctx.lineTo(w/2, h/2);
        ctx.lineTo(0, 0);
        ctx.fill();
      }
    );

    let corners = this.clipLayers(
      this.image, 2,
      i => (connection & 0b10000) || (
        ((connection & (0b1000 >> i)) == 0) ||
        ((connection & (0b1000 >> (i-1 + 4)%4)) == 0)
      ),
      (ctx, w, h) => ctx.fillRect(0, 0, w/2, h/2)
    );

    return [
      [this.image, 0, 0, w/3, h],
      [borders, 0, 0, w/3, h],
      [corners, 0, 0, w/3, h]
    ];
  }
}

const PIECES = ["z", "l", "o", "s", "i", "j", "t", "ghost", "hold", "gb", "dgb", "topout"];

export default class TwelveBlocksSplicer {
  constructor(images) {
    this.format = '12-blocks';
    this.images = images;
  }
  stats() { throw new Error("Not implemented"); }
  set() { throw new Error("Not implemented"); }
  get(piece, connection, defaultConnection=0) {
    let img = this.images[PIECES.indexOf(piece)];
    if (!img) return [];
    return [[img, 0, 0, img.width, img.height]];
  }
}

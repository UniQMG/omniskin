const PIECES = ["z", "l", "o", "s", "i", "j", "t", "ghost", "hold", "gb", "dgb", "topout"];

export default class TetrioGapFixerSplicer {
  constructor(image, actualGap) {
    this.format = 'tetrioraster';
    this.image = image;
    this.actualGap = actualGap;
  }

  get images() { return [this.image]; }
  stats() { throw new Error("Not implemented"); }
  set() { throw new Error("Not implemented"); }

  get(piece, _connection=0) {
    let { width, height } = this.image;

    let index = PIECES.indexOf(piece);
    let blockSize = this.image.height;
    let gapSize = this.actualGap;

    let x = (blockSize + gapSize) * index;
    let w = blockSize;

    return [[this.image, x, 0, w, height]];
  }
}
